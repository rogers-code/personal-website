import * as React from 'react';
import Box from '@mui/material/Box';
import Fab from '@mui/material/Fab';
import HomeIcon from '@mui/icons-material/Home';
import BookIcon from '@mui/icons-material/Book';
import ComputerIcon from '@mui/icons-material/Computer'
import QuestionMarkIcon  from '@mui/icons-material/QuestionMark';
import "./NavBar.css"

import { useLocation } from 'react-router-dom'


export default function NavBar(){

    // Use different css class if on the root route.
    const location = useLocation();
    let navClass = "NavBar";
    if (location.pathname==='/'){
        navClass = "NavBarHome";
    }
    else{
        navClass = "NavBar";
    }
    
    return (
        <div className={navClass}>
        <ul>
            <li>
                <Fab color='primary' aria-label='add' href='/'>
                    <HomeIcon/>

                </Fab>
            </li>
            <li>
                <Fab color='secondary' href='bookclub'>
                    <BookIcon/>
                </Fab>
            </li>
            <li>
                <Fab color='inherit' href='tech'>
                    <ComputerIcon/>
                </Fab>
            </li>
            <li>
                <Fab color='info' href='about'>
                    <QuestionMarkIcon/>
                </Fab>
            </li>
        </ul>
        </div>
    )
}