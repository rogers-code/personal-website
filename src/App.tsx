import React, { useState, useEffect } from 'react';
import * as ReactDOM from "react-dom/client";
import './App.css';
import { Outlet } from 'react-router-dom';
import NavBar from './NavBar';

export default function App() {
    return (
      <>
      <NavBar/>
      <Outlet/>
      </>
    );
  }