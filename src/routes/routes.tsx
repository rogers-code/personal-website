import React from "react";
import ReactDOM from "react-dom";
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import App from "../App";
import Home from "../pages/home/home";
import BookClub from "../pages/bookclub/bookclub";

const router = createBrowserRouter([
  {
    path: "/",
    element: <App/>,
    children:[
      {path:"", element: <Home/>},
      {path:"/bookclub", element: <BookClub/>}
    ]
  },
]);

export default router