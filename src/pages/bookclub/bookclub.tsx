import BookTable from "./BookTable";

export default function BookClub(){
    return(
        <div>
            <h1>Rogers Bookclub</h1>
            <BookTable/>
        </div>
    )
}