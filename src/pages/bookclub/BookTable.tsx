import React, { useState, useEffect } from 'react';
import { DataGrid, GridRowsProp, GridColDef } from '@mui/x-data-grid';

// Define an interface for the book object
interface Book {
    title: string;
    author: string;
    year_published: number;
    genre: string;
    page_length: number;
    date_read: string;
    rating: number;
  }
  
  export default function BookTable() {
    const [books, setBooks] = useState<Book[]>([]); // Annotate the type as Book[]
  
    useEffect(() => {
      const fetchData = async () => {
        try {
          const response = await fetch('/bookclubbooks.json');
          const data = await response.json();
          setBooks(data.books_read);
        } catch (error) {
          console.error('Error fetching data: ', error);
        }
      };
  
      fetchData();
    }, []);

    const columns: GridColDef[] = [
      { field: 'title', headerName: 'Title', width: 200 },
      { field: 'author', headerName: 'Author', width: 150 },
      { field: 'year_published', headerName: 'Year Published', width: 150 },
      { field: 'genre', headerName: 'Genre', width: 150 },
      { field: 'page_length', headerName: 'Page Length', width: 150 },
      { field: 'date_read', headerName: 'Date Read', width: 200 },
      { field: 'rating', headerName: 'Rating', width: 150 },
    ];
    return (
      <div>
        <DataGrid rows={books} columns={columns} />
      </div>
    );
  }